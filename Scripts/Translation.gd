extends Control

const ALL_GROUP = "<All lines>"
const DEFAULT_GROUP = "<Unassigned>"

var path

var lines = {}
var groups = []
var all_group_lines = {}
var untranslated_count = 0 setget set_progress

var current_group
var current_line = 0 setget set_line
var group_lines = []
var group_untranslated_count = 0 setget set_group_progress

var last_text = ""
var dirty = false setget set_dirty
var warned_action
var unsaved_progress = false setget set_unsaved

func _ready():
	$RightSide/WarningCheckBox.pressed = Config.unsaved_warning
	
	var file = File.new()
	file.open(Config.last_file, file.READ)
	var readlines = []
	
	var line = file.get_line()
	line = file.get_line()
	
	while line:
		if not (line.begins_with("}") or line.begins_with("\n")): readlines.append(line)
		line = file.get_line()
	
	for line2 in readlines:
		var element = {}
		
		var part = line2.split("[\"")
		if part[0].begins_with("--"):
			element.untranslated = true
			untranslated_count += 1
		
		part = part[1].split("\"] = \"")
		var element_name = part[0]
		lines[element_name] = element
		
		part = part[1].split("\", -- ")
		var element_groups = [DEFAULT_GROUP]
		
		if part.size() > 1:
			element_groups = Array(part[1].split(", "))
			if element_groups[0] == "" or element_groups[0].begins_with(" "): element_groups = [DEFAULT_GROUP]
		else:
			part[0] = part[0].left(part[0].find("\""))
		
		element_groups.insert(0, ALL_GROUP)
		
		for group in element_groups:
			if !group in groups:
				groups.append(group)
				all_group_lines[group] = []
			
			all_group_lines[group].append(element_name)
		
		element.groups = element_groups
		element.translated = part[0].replace("\\\"", "\"")
	
	for group in groups:
		$List.add_item(group)
		
		var untranslated = false
		for line2 in all_group_lines[group]:
			if lines[line2].has("untranslated"):
				untranslated = true
				break
		
		if untranslated:
			$List.set_item_icon($List.get_item_count()-1, preload("res://Sprites/Untranslated.png"))
	
	$List.select(0)
	select_group(0)
	
	self.untranslated_count = untranslated_count

func select_group(index):
	current_group = groups[index]
	group_lines = all_group_lines[current_group]
	
	group_untranslated_count = 0
	
	for line in lines.keys():
		if current_group == "All lines" or current_group in lines[line].groups:
			if lines[line].has("untranslated"): group_untranslated_count += 1
	
	self.group_untranslated_count = group_untranslated_count
	self.current_line = 0

func set_line(line):
	current_line = min(max(0, line), group_lines.size()-1)
	last_text = lines[group_lines[current_line]].translated
	$RightSide/LineNumber.text = "%d out of %d" % [current_line+1, group_lines.size()]
	
	$RightSide/Buttons/Previous.disabled = (line == 0)
	$RightSide/Buttons/Next.disabled = (line == group_lines.size()-1)
	
	$RightSide/Texts/Original.text = group_lines[current_line]
	$RightSide/Texts/Translated.text = lines[group_lines[current_line]].translated
	$RightSide/Texts/Translated.clear_undo_history()
	

func previous_line(forced = false):
	if !forced and Config.unsaved_warning and dirty:
		warned_action = "previous_line"
		$Warning.popup()
	else:
		self.current_line -= 1

func next_line(forced = false):
	if !forced and Config.unsaved_warning and dirty:
		warned_action = "next_line"
		$Warning.popup()
	else:
		self.current_line += 1

func next_untranslated(forced = false):
	if !forced and Config.unsaved_warning and dirty:
		warned_action = "next_untranslated"
		$Warning.popup()
	else:
		for i in group_lines.size():
			if lines[group_lines[i]].has("untranslated"):
				self.current_line = i
				return
			
func on_translated_text_changed():
	self.dirty = ($RightSide/Texts/Translated.text != last_text)

func set_dirty(_dirty):
	dirty = _dirty
	
	if dirty:
		$RightSide/LineHeader.text = "Current line*"
		$RightSide/LineHeader.add_color_override("font_color", Color(1, 0, 0))
	else:
		$RightSide/LineHeader.text = "Current line"
		$RightSide/LineHeader.add_color_override("font_color", Color(1, 1, 1))

func set_unsaved(unsaved):
	unsaved_progress = unsaved
	
	if unsaved_progress:
		$RightSide/Save.text = "Save*"
		$RightSide/Save.add_color_override("font_color", Color(1, 0, 0))
	else:
		$RightSide/Save.text = "Save"
		$RightSide/Save.add_color_override("font_color", Color(1, 1, 1))

func set_group_progress(untranslated):
	group_untranslated_count = untranslated
	$RightSide/GroupProgress.text = "Group translation progress: %d / %d (%.2f%%)" % [group_lines.size() - group_untranslated_count, group_lines.size(), float(group_lines.size() - group_untranslated_count) * 100 / group_lines.size()]
	$RightSide/Buttons/Untranslated.disabled = (untranslated == 0)
	if untranslated == 0: $List.set_item_icon(groups.find(current_group), null)

func set_progress(untranslated):
	untranslated_count = untranslated
	$RightSide/TotalProgress.text = "Total translation progress: %d / %d (%.2f%%)" % [lines.size() - untranslated_count, lines.size(), float(lines.size() - untranslated_count) * 100 / lines.size()]

func copy_english():
	$RightSide/Texts/Translated.text = $RightSide/Texts/Original.text

func store_string():
	if dirty:
		var line = lines[group_lines[current_line]]
		line.translated = $RightSide/Texts/Translated.text
		
		if line.has("untranslated"):
			line.erase("untranslated")
			self.group_untranslated_count -=1
			self.untranslated_count -= 1
		
		self.unsaved_progress = true
		last_text = $RightSide/Texts/Translated.text
		self.dirty = false

func save():
	var file = File.new()
	if file.open(Config.last_file, file.WRITE) == OK:
		file.store_line("locale = {")
		
		for line_name in lines.keys():
			var line = lines[line_name]
			var builder = PoolStringArray()
			
			if line.has("untranslated"): builder.append("--    ")
			builder.append("    [\"")
			
			builder.append(line_name)
			builder.append("\"] = \"")
			
			builder.append(line.translated.replace("\"", "\\\""))
			builder.append("\",")
			
			if !line.groups.has(DEFAULT_GROUP):
				builder.append(" -- ")
				
				var groups = PoolStringArray()
				for group in line.groups:
					if group != ALL_GROUP:
						groups.append(group)
				
				builder.append(groups.join(", "))
			file.store_line(builder.join(""))
		file.store_line("}")
		
		self.unsaved_progress = false
	else:
		$Error.popup()

func change_file():
	Config.last_file = ""
	get_tree().change_scene("res://Scenes/FileSelect.tscn")

func on_warning_changed(value):
	Config.unsaved_warning = value
	Config.save()

func warning_yes():
	call(warned_action, true)
	$Warning.visible = false

func warning_save():
	store_string()
	call(warned_action, true)
	$Warning.visible = false

func warning_no():
	$Warning.visible = false


func close_error():
	$Error.visible = false
