extends Panel

func _ready():
	$FileDialog.current_path = Config.last_path
	
	if Config.last_file != "":
		call_deferred("load_file", Config.last_file)

func on_load(): $FileDialog.popup()
func on_selected(path): load_file(path, true)

func load_file(path, from_dialog = false):
	if verify_file(path):
		if from_dialog:
			Config.last_path = path.left(path.length() - path.get_file().length())
			Config.last_file = path
			Config.save()
		
		get_tree().change_scene("res://Scenes/Translation.tscn")
	else:
		$FileError/Filename.text = path
		$FileError.popup()

func verify_file(path):
	var file = File.new()
	if file.open(path, file.READ) == OK:
		var readlines = []
		
		var line = file.get_line()
		line = file.get_line()
		
		while line:
			if not (line.begins_with("}") or line.begins_with("\n")): readlines.append(line)
			line = file.get_line()
		
		for line2 in readlines:
			var element = {}
			
			var part = line2.split("[\"")
			if part.size() < 2: return
			
			if part[0].begins_with("--"):
				element.untranslated = true
			
			part = part[1].split("\"] = \"")
			var element_name = part[0]
			
			if part.size() < 2: return
			part = part[1].split("\", -- ")
			var element_groups = ["?"]
			
			if part.size() > 1:
				element_groups = Array(part[1].split(", "))
				if element_groups[0] == "" or element_groups[0].begins_with(" "): element_groups = ["?"]
			else:
				part[0] = part[0].left(part[0].find("\""))
			
			element_groups.insert(0, "!")
			
			element.groups = element_groups
			element.translated = part[0]
	
		return true

func ok_pressed():
	$FileError.visible = false
