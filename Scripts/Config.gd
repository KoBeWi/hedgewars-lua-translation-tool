extends Node

var config_loaded = false
var last_path = ProjectSettings.globalize_path("res://")
var unsaved_warning = true
var last_file = ""

func _ready():
	if !config_loaded:
		config_loaded = true
		
		var file = File.new()
		if file.open("user://config", file.READ) == OK:
			var data = file.get_as_text().split("\n")
			if data.size() < 3: return
			
			last_path = data[0]
			last_file = data[1]
			unsaved_warning = data[2] == "True"

func save():
	var file = File.new()
	file.open("user://config", file.WRITE)
	file.store_line(last_path)
	file.store_line(last_file)
	file.store_line(str(unsaved_warning))
